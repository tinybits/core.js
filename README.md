# Core.js
## Version 0.1.0a

Tiny Javascript-library built on top of Backbone, jQuery and Handlebars. Only jQuery and Backbone is required for most functionality.

The goals are to keep boiler-plate code to the minimum for common tasks like creating tables, doing AJAX (also authenticated), changing pages etc.

The library assumes that there's an API-service somewhere. The AJAX-calls works with CORS.

`core.js` must be included, but the remaining libraries are optional.

## Installation

Look at example-index.html for information on how this must look.

## Todo

This documentation lacks a lot, so does the library! It is *not* production ready and you may expect significant changes.