(function() {
	Core.paginate = function(res) {
		var nav = $('<nav>');
		var pagi = $('<ul class="pagination">').appendTo(nav);
		var prev = $('<li><a href="#"><span>&laquo;</span></a></li>');
		console.log(res.current_page);
		if (res.current_page == 1) prev.addClass('disabled');
		else {
			prev.click(function() { pagi.trigger('paginate', [ res.current_page-1 ]) })
		}
		pagi.append(prev);

		for(var i = 1; i<=res.last_page; i++) {
			var page = $('<li><a href="#'+i+'">'+i+'</a></li>');
			if (res.current_page == i) page.addClass('active');
			else {
				page.click(function() { pagi.trigger('paginate', [ Number($(this).children().attr('href').substr(1)) ]) })
			}
			pagi.append(page);
		}

		var next = $('<li><a href="#"><span>&raquo;</span></a></li>');
		if (res.last_page == res.current_page) next.addClass('disabled');
		else {
			next.click(function() { pagi.trigger('paginate', [ res.current_page+1 ]) })
		}
		pagi.append(next);

		nav.on('paginate', function(ev, idx) {
			nav.find('li.active').removeClass('active');
			nav.find('li:nth-child('+(idx+1)+')').addClass('active');
			Core.loading(true);
		});

		return nav;
	};
})();