(function() {
	Handlebars.registerHelper( 'dateHuman', function(options) {
		var date = moment($.trim(options.fn(this)));
		return new Handlebars.SafeString( date.calendar() );
	});

	Handlebars.registerHelper( 'dateFull', function(options) {
		var date = moment($.trim(options.fn(this)));
		return new Handlebars.SafeString( date.calendar() );
	});

	Handlebars.registerHelper( 'dkk', function(options) {
		var price = (Number($.trim(options.fn(this))));
		return new Handlebars.SafeString( "DKK " + number_format(price / 100, 2, ',', '.') );
	});

	Handlebars.registerHelper( 'progressbar', function(options) {
		var prices = options.fn(this).split('/');
		if (prices.length != 2) return;
		var p = $('<div class="progress">');
		prices[0] = Number($.trim(prices[0]));
		prices[1] = Number($.trim(prices[1]));
		if (prices[1] < 1) prices[1] = 1;
		var percentage = prices[0] / prices[1];
		if (percentage > 100) {
			percentage = 100;
		}

		var inner = $('<div class="progress-bar">').css({width: percentage*100+'%'});
		if (percentage >= .90) {
			inner.addClass('progress-bar-danger');
		}
		else if (percentage >= .75) {
			inner.addClass('progress-bar-warning');
		} else {
			inner.addClass('progress-bar-success');
		}


		if (percentage > 0.25) inner.text(number_format(percentage*100)+' %');
		p.append(inner);

		return new Handlebars.SafeString( p.get(0).outerHTML );
	});
})();