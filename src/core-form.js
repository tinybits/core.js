(function($) {
	$.fn.coreForm = function() {
		var alertEl = $('<div class="alert hidden">');
		var form = $(this);
		var submitBtn = form.find('.btn').filter('[type="submit"]');


		function handleFormSuccess(res) {
			alertEl.attr('class', 'alert alert-success').text(res.message || 'Super! Videre med os.');
			var redirect = res.redirect || form.attr('data-redirect');
			if (redirect) Core.router.navigate(redirect, { trigger: true });

			form.trigger('success', [res]);
		}


		function handleFormError(res) {
			if (res.status >= 500) {
				var msg = _.isObject(res.responseJSON) ? res.responseJSON.message : null;
				if (!msg) msg = "Der opstod en fejl på serveren. Prøv venligst igen senere. Vi beklager meget!";
				alertEl.attr('class', 'alert alert-danger').text(msg);
			}
			else if (res.status >= 400) {
				var res = res.responseJSON;
				if (!msg) msg = "Der var en fejl i det medsendte indhold. Fejlene er fremhævet nedenfor.";
				alertEl.attr('class', 'alert alert-warning').text(msg);

				_.each(res.errors, function(msgs, field) {
					var inputEl = form.find('[name="'+field+'"]');

					if (!inputEl.length) {
						var moreErr = alertEl.find('.more-errors');
						if (!moreErr.length) {
							moreErr = $('<div class="more-errors" style="margin-top: 10px">Bemærk desuden følgende:<ul style="margin-bottom: 0;"></ul></div>');
							alertEl.append(moreErr);
						}
						var ul = moreErr.find('ul');
						_.each(msgs, function(msg) {
							ul.append($('<li>').text(msg));
						});

					} else {
						var fgroup = inputEl.parents('.form-group');
						fgroup.addClass('has-error');

						// error messages
						_.each(msgs, function(msg) {
							msg = msg[0].toUpperCase() + msg.substr(1);
							$('<div class="help-block error-block">').text(msg).insertAfter(inputEl);
						});
					}
				});
			}

			setLoading(false);
		}

		function setLoading(isLoading)
		{
			if (isLoading !== false) {
				if (!submitBtn.data('loading-text')) submitBtn.data('loading-text', 'Indlæser...');
				submitBtn.button('loading');
			} else {
				submitBtn.button('reset');
			}
		}


		form.submit(function(ev) {
			ev.preventDefault();
			setLoading();

			// do we have an alert container?
			var alertOuter = $('.coreform-alert');
			if (alertOuter.length) {
				alertOuter.prepend(alertEl);
			} else {
				$(this).prepend(alertEl);
			}

			Core.post( $(this).attr('action'), $(this).serialize(), $(this).attr('method')).
				success(handleFormSuccess).
				fail(handleFormError);

			// reset form from last time
			form.find('.form-group.has-error').removeClass('has-error').each(function() {
				$(this).find('.help-block.error-block').remove();
			});
			alertEl.addClass('hidden');
		});


		return this;
	};

	$.fn.coreConfirmButton = function(clickCb) {
		$(this).each(function() {
			var origInner = $(this).html();
			var el = $(this);

			var cancelWaitTimer = null;
			function cancelWait()
			{
				el.html(origInner).tooltip('hide').prop('disabled', false);
				initialState = true;
			}

			var initialState = true;
			$(this).click(function(ev) {
				if (initialState) { // start
					ev.preventDefault();
					initialState = false;
					cancelWaitTimer = setTimeout(cancelWait, 6000); // cancel if nothing happens
					$(this).text( $(this).data('confirm-text') );
					$(this).tooltip({ 'trigger': 'manual', title: $(this).data('confirm-tooltip') || 'Tryk igen for at bekræfte' }).tooltip('show')
				} else { // success!
					clearTimeout(cancelWaitTimer);
					$(this).text($(this).data('working-text') || 'Arbejder...');
					$(this).prop('disabled', true);
					$(this).tooltip('hide');
					if (clickCb) clickCb.apply(this, [ev]);
				}
			});
		});

		return this;
	};
})(window.jQuery);