(function () {
	if (!window.Core) window.Core = {};

	Core.apiUrl = Core.apiUrl || "http://demo-api.com";
	Core.bodyElement =Core.bodyElementement || $('#inner');
	Core.loadingElement = Core.loadingElement || $('#loading');
	Core.tplBaseUrl = Core.tplBaseUrl || "/tpl/";

	var auth_extra_data = {};
	window.appendAuthData = function appendAuthData(data) {
		if (window.lscache && lscache.get('access_token')) {
			auth_extra_data.access_token = lscache.get('access_token');

			if (!data) {
				data = $.param(auth_extra_data);
			}
			else if (_.isString(data)) {
				data = data.length == 0 ? $.param(auth_extra_data) : data + '&' + $.param(auth_extra_data);
			} else if (_.isObject(data)) {
				data = _.extend(data, auth_extra_data);
			}
		}

		return data;
	};

	/**
	 * Post data to the API. Returns a jqXHR-object.
	 */
	Core.post = function (path, data, type) {
		data = appendAuthData(data);

		return $.ajax({
			crossDomain: true,
			type: type || "POST",
			url: Core.apiUrl + path,
			data: data,
			dataType: 'json'
		}).fail(handleAuthFail);
	};

	/**
	 * GET data from the API. Returns a jqXHR-object.
	 */
	Core.get = function (path, data) {
		data = appendAuthData(data);

		return $.ajax({
			type: "GET",
			url: Core.apiUrl + path,
			data: data,
			dataType: 'json'
		}).fail(handleAuthFail);
	};

	/**
	 * When auth fail, redirect to frontpage.
	 *
	 * @TODO should redirect somewhere customizable OR perhaps be event driven.
	 */
	function handleAuthFail(ev, res) {
		if (ev.status == 401) {
			lscache.remove('access_token');
			Core.router.navigate('/', {trigger: true});
			window.location.reload();
			return;
		}
	}


	/**
	 * Fetch a template file from the /tpl/-folder.
	 * @param path
	 * @returns {*}
	 */
	Core.getTpl = function (path) {
		return $.get(Core.tplBaseUrl + path);
	};


	/**
	 * Open a page.
	 *
	 * @param html
	 * @param animateChange
	 * @returns {*|jQuery|HTMLElement}
	 */
	Core.openPage = function openPage(html, animateChange) {
		var closingPages = Core.bodyElement.children('.page').not('.fade-out');
		if (animateChange !== false) animateChange = true;

		// existing page: fade out
		if (animateChange) {
			closingPages.addClass('fade-out');
			setTimeout(function () {
				closingPages.remove();
			}, 500);
		} else {
			closingPages.remove();
		}

		// new page
		var page = $('<div class="page">');
		page.append(html);
		page.appendTo(Core.bodyElement);

		// fade in
		if (animateChange) {
			page.addClass('fade-in');
			setTimeout(function () {
				page.removeClass('fade-in');
			}, 500);
		}

		// loading element still existing?
		Core.loading(false);

		return page;
	};


	/**
	 * Set the loading state.
	 *
	 * @TODO implement a counter, so when set to loading multiple times, equal amount of times must be subtracted for loading to be complete.
	 */
	Core.loading = function (loading) {
		if (loading === false) {
			Core.loadingElement.addClass('done');
		} else {
			Core.loadingElement.removeClass('done');
			$(':focus').blur();
		}
	};


	/**
	 * Various utility methods.
	 */
	Core.util = {
		is_email: function validateEmail(email) {
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			return re.test(email);
		},

		// thanks to: http://phpjs.org/functions/number_format/
		number_format: function number_format(number, decimals, dec_point, thousands_sep) {

			number = (number + '')
				.replace(/[^0-9+\-Ee.]/g, '');
			var n = !isFinite(+number) ? 0 : +number,
				prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
				sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
				dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
				s = '',
				toFixedFix = function (n, prec) {
					var k = Math.pow(10, prec);
					return '' + (Math.round(n * k) / k)
							.toFixed(prec);
				};
			// Fix for IE parseFloat(0.55).toFixed(0) = 0;
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
				.split('.');
			if (s[0].length > 3) {
				s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
			}
			if ((s[1] || '')
					.length < prec) {
				s[1] = s[1] || '';
				s[1] += new Array(prec - s[1].length + 1)
					.join('0');
			}
			return s.join(dec);
		},

		// thanks to: http://phpjs.org/functions/ltrim/
		ltrim: function ltrim(str, charlist) {
			charlist = !charlist ? ' \\s\u00A0' : (charlist + '')
				.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
			var re = new RegExp('^[' + charlist + ']+', 'g');
			return (str + '')
				.replace(re, '');
		},

		// thanks to: http://phpjs.org/functions/rtrim/
		rtrim: function rtrim(str, charlist) {
			charlist = !charlist ? ' \\s\u00A0' : (charlist + '')
				.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1');
			var re = new RegExp('[' + charlist + ']+$', 'g');
			return (str + '')
				.replace(re, '');
		},

		trim: function trim(str, charlist) {
			return Cure.util.ltrim( Core.util.rtrim(str, charlist), charlist);
		}
	};


})();