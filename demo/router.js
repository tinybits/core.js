(function() {
	var base_url = "demo";
	var routes = {};
	function add_route(route, name)
	{
		if (route && route.length) route = '/'+route;
		else route = '';

		routes[base_url+route+'(/)'] = name;
	}

	add_route(null, 'hello');

	Core.router = new Backbone.Router({
		routes: routes
	});

	// use backbone routing for regular links
	$('body').on('click', 'a[href^="/demo/"]', function(ev) {
		ev.preventDefault();
		Core.router.navigate( $(this).attr('href'), {trigger: true} );
		Core.loading(true);
	});
})();